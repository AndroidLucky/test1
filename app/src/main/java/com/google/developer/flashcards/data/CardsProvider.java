package com.google.developer.flashcards.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.media.UnsupportedSchemeException;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import static com.google.developer.flashcards.data.DatabaseContract.TABLE_FLASHCARDS;

public class CardsProvider extends ContentProvider {
    private static final String TAG = CardsProvider.class.getSimpleName();

    private static final int FLASHCARDS = 200;
    private static final int FLASHCARDS_WITH_ID = 201;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,
                TABLE_FLASHCARDS,
                FLASHCARDS);

        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY,
                TABLE_FLASHCARDS + "/#",
                FLASHCARDS_WITH_ID);
    }

    private CardsDBHelper mCardsDBHelper;

    @Override
    public boolean onCreate() {
        mCardsDBHelper = new CardsDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        //TODO: Implement the query function

final SQLiteDatabase db=mCardsDBHelper.getReadableDatabase();

        int match=sUriMatcher.match(uri);
Cursor retcursor=null;
        switch (match)
        {
            case FLASHCARDS:
            {
                retcursor=db.query(TABLE_FLASHCARDS,projection,selection,selectionArgs,null,null,sortOrder);
           break;
            }

            case FLASHCARDS_WITH_ID:
            {
                retcursor=db.query(TABLE_FLASHCARDS,projection,selection,selectionArgs,null,null,sortOrder);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri"+uri);

        }

        retcursor.setNotificationUri(getContext().getContentResolver(),uri);
        return retcursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        //TODO: Implement the insert function
final SQLiteDatabase db=mCardsDBHelper.getWritableDatabase();

        int match=sUriMatcher.match(uri);
Uri returnuri;


switch (match)
{
    case FLASHCARDS:

    {
long id=db.insert(TABLE_FLASHCARDS,null,values);

        if(id>0)
        {
            returnuri= ContentUris.withAppendedId(DatabaseContract.CONTENT_URI,id);

        }
        else
        {
            throw new android.database.SQLException("Failed to insert row into " + uri);
        }
    }

    break;

    default:
        throw new UnsupportedOperationException("Unknown uri: " + uri);

}

        getContext().getContentResolver().notifyChange(uri, null);


        return returnuri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("This provider does not support deletion");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("This provider does not support updates");
    }
}
